use lazy_static::lazy_static;
use std::{collections::HashMap, io::BufRead};

#[macro_use]
extern crate scan_fmt;

fn read_input() -> Vec<i64> {
    std::io::stdin()
        .lock()
        .lines()
        .map(|l| {
            let l = l.unwrap();

            let (_, pos) = scan_fmt!(&l, "Player {} starting position: {}", i64, i64).unwrap();

            pos
        })
        .collect()
}

fn roll(order: i64) -> i64 {
    (0..3).map(|roll| (roll + order * 3) % 100 + 1).sum()
}

fn play(starting_positions: &Vec<i64>) -> i64 {
    let mut scores = vec![0; 2];
    let mut positions = starting_positions.clone();

    let mut turn: usize = 0;

    while scores.iter().all(|&v| v < 1000) {
        let score = roll(turn as i64);
        positions[turn % 2] = (positions[turn % 2] + score - 1) % 10 + 1;
        scores[turn % 2] += positions[turn % 2];

        turn += 1;
    }

    turn as i64 * 3 * scores.iter().min().unwrap()
}

lazy_static! {
    static ref SPLIT_COUNTS: [i64; 10] = {
        let mut counts = [0; 10];
        for i in 1..=3 {
            for j in 1..=3 {
                for k in 1..=3 {
                    counts[i + j + k] += 1;
                }
            }
        }
        counts
    };
}

fn play_quantum(
    cache: &mut HashMap<(usize, [i64; 2], [i64; 2]), [i64; 2]>,
    turn: usize,
    scores: [i64; 2],
    positions: [i64; 2],
) -> [i64; 2] {
    if let Some(i) = scores.iter().position(|&v| v >= 21) {
        let mut result = [0; 2];
        result[i] = 1;
        return result;
    }

    if let Some(&wins) = cache.get(&(turn, scores, positions)) {
        return wins;
    }

    let mut wins = [0; 2];

    for i in 3..=9 {
        let mut new_scores = scores.clone();
        let mut new_positions = positions.clone();
        new_positions[turn] = (new_positions[turn] + i - 1) % 10 + 1;
        new_scores[turn] += new_positions[turn];
        let sub_wins = play_quantum(cache, (turn + 1) % 2, new_scores, new_positions);

        for j in 0..2 {
            wins[j] += sub_wins[j] * SPLIT_COUNTS[i as usize];
        }
    }

    cache.insert((turn, scores, positions), wins);

    return wins;
}

fn part2(starting_positions: &Vec<i64>) -> i64 {
    let mut cache = HashMap::new();
    let mut positions = [0; 2];
    for i in 0..2 {
        positions[i] = starting_positions[i];
    }

    let wins = play_quantum(&mut cache, 0, [0; 2], positions);

    *wins.iter().max().unwrap()
}

fn main() {
    let starting_positions = read_input();

    println!("part1: {}", play(&starting_positions));
    println!("part2: {}", part2(&starting_positions));
}
