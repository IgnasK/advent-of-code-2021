use std::io::{self, BufRead};

fn read_input() -> Vec<i32> {
    io::stdin().lock().lines().map(|l| l.unwrap().parse::<i32>().unwrap()).collect()
}

fn part1(input: &Vec<i32>) -> i32 {
    input.iter().zip(input.iter().skip(1)).filter(|&(x, y)| y > x).count() as i32
}

fn part2(input: &Vec<i32>) -> i32 {
    let mut sliding_sums = Vec::<i32>::new();

    sliding_sums.push(input[0] + input[1] + input[2]);

    for i in 3..input.len() {
        sliding_sums.push(
            sliding_sums.last().unwrap() +
            input[i] -
            input[i-3]
        )
    }

    part1(&sliding_sums)
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part1: {}", part2(&input));
}