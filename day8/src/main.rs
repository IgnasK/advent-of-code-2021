use std::{collections::HashMap, io::BufRead};
use itertools::Itertools;
use lazy_static::lazy_static;

type Number = String;

#[derive(Debug)]
struct Input {
    examples: Vec<Number>,
    numbers: Vec<Number>
}

fn read_input() -> Vec<Input> {
    std::io::stdin().lock().lines().map(|line| {
        let line = line.unwrap();

        let mut parts = line.split(" | ");

        let examples = parts.next().unwrap().split(" ").map(|v| v.to_owned()).collect();
        let numbers = parts.next().unwrap().split(" ").map(|v| v.to_owned()).collect();

        Input{
            examples: examples,
            numbers: numbers,
        }
    }).collect()
}

fn part1(inputs: &Vec<Input>) -> i32 {
    inputs.iter().map(|i| i.numbers.iter().filter(|number| {
        let length = number.len();
        length == 2 || length == 3 || length == 4 || length == 7
    }).count() as i32).sum()
}

lazy_static! {
    static ref MAPPING: HashMap<&'static str, i32> = HashMap::from([
        ("abcefg", 0),
        ("cf", 1),
        ("acdeg", 2),
        ("acdfg", 3),
        ("bcdf", 4),
        ("abdfg", 5),
        ("abdefg", 6),
        ("acf", 7),
        ("abcdefg", 8),
        ("abcdfg", 9),
    ]);
}

fn parse(number: &Number) -> Option<i32> {
    let sorted: String = number.chars().sorted().collect();
    MAPPING.get(&sorted as &str).cloned()
}

fn rename(number: &Number, permutation: &Vec<char>) -> Number {
    number.chars().map(|c| permutation[(c as u32 - 'a' as u32) as usize]).collect()
}

fn solve(input: &Input) -> i32 {
    let perm = "abcdefg".chars().permutations(7).find(|perm| {
        input.examples.iter().all(|example| {
            parse(&rename(example, perm)).is_some()
        })
    }).unwrap();

    let mut result = 0;

    for n in input.numbers.iter() {
        result *= 10;
        result += parse(&rename(n, &perm)).unwrap();
    }

    result
}

fn part2(inputs: &Vec<Input>) -> i32 {
    inputs.iter().map(|input| {
        solve(input)
    }).sum()
}

fn main() {
    let inputs = read_input();
    println!("part1: {}", part1(&inputs));
    println!("part2: {}", part2(&inputs));
}
