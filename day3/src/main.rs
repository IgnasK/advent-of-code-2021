use std::io::{self, BufRead};

fn read_input() -> (Vec<usize>, usize) {
    let mut num_width: usize = 0;
    let mut input: Vec<usize> = vec![];

    for line in io::stdin().lock().lines() {
        let line = line.unwrap();
        num_width = line.len();
        let num = usize::from_str_radix(&line, 2).unwrap();
        input.push(num)
    }

    (input, num_width)
}

fn part1(input: &Vec<usize>, width: usize) -> usize {
    let mut common: usize = 0;
    for i in 0..width {
        let one_count: usize = input.iter().map(|f| (f >> i) & 1).sum();
        if 2*one_count > input.len() {
            common |= 1 << i;
        }
    }

    let uncommon = !common & ((1 << width) - 1);

    common * uncommon
}

fn oxygen_rating(input: &Vec<usize>, width: usize) -> usize {
    let mut values = input.to_owned();

    for i in (0..width).rev() {
        let one_count: usize = values.iter().map(|f| (f >> i) & 1).sum();
        let bit_to_keep = if 2*one_count >= values.len() {
            1
        } else {
            0
        };
        values.retain(|&f| (f >> i) & 1 == bit_to_keep);
        if values.len() == 1 {
            return values[0];
        }
    }

    values[0]
}

fn scubber_rating(input: &Vec<usize>, width: usize) -> usize {
    let mut values = input.to_owned();

    for i in (0..width).rev() {
        let one_count: usize = values.iter().map(|f| (f >> i) & 1).sum();
        let bit_to_keep = if 2*one_count >= values.len() {
            0
        } else {
            1
        };
        values.retain(|&f| (f >> i) & 1 == bit_to_keep);
        if values.len() == 1 {
            return values[0];
        }
    }

    values[0]
}

fn part2(input: &Vec<usize>, width: usize) -> usize {
    let oxygen = oxygen_rating(input, width);
    let scrubber = scubber_rating(input, width);

    oxygen * scrubber
}

fn main() {
    let (numbers, width) = read_input();

    println!("part1: {}", part1(&numbers, width));
    println!("part2: {}", part2(&numbers, width));
}
