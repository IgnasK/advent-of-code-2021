use std::{collections::{HashMap, HashSet}, io::Read};

use scan_fmt::scan_fmt;

#[derive(Debug)]
struct Area {
    min_x: i32,
    max_x: i32,
    min_y: i32,
    max_y: i32,
}

fn read_input() -> Area {
    let mut input = String::new();

    std::io::stdin().lock().read_to_string(&mut input).unwrap();

    let (min_x, max_x, min_y, max_y) = scan_fmt!(
        &input,
        "target area: x={}..{}, y={}..{}",
        i32,
        i32,
        i32,
        i32
    )
    .unwrap();

    Area {
        min_x,
        max_x,
        min_y,
        max_y,
    }
}

fn part1(input: &Area) -> i32 {
    let mut allowed_steps = HashSet::<i32>::new();

    for dx in 0..input.max_x + 1 {
        let mut x = 0;
        let mut vel = dx;
        let mut steps = 0;

        while x <= input.max_x && steps < 1000 {
            x += vel;
            if vel > 0 {
                vel -= 1;
            } else if vel < 0 {
                vel += 1;
            }
            steps += 1;

            if x >= input.min_x && x <= input.max_x {
                allowed_steps.insert(steps);
            }
        }
    }

    for dv in (-100..10000).rev() {
        let mut y = 0;
        let mut vel = dv;
        let mut steps = 0;
        let mut max_reached = 0;

        while y >= input.min_y {
            y += vel;
            if y > max_reached {
                max_reached = y;
            }
            vel -= 1;
            steps += 1;

            if y >= input.min_y && y <= input.max_y && allowed_steps.contains(&steps) {
                return max_reached;
            }
        }
    }

    0
}

fn part2(input: &Area) -> usize {
    let mut visit_count = HashMap::<i32, Vec<i32>>::new();
    let mut combinations = 0;

    for dx in 0..input.max_x + 1 {
        let mut x = 0;
        let mut vel = dx;
        let mut steps = 0;

        while x <= input.max_x && steps < 1000 {
            x += vel;
            if vel > 0 {
                vel -= 1;
            } else if vel < 0 {
                vel += 1;
            }
            steps += 1;

            if x >= input.min_x && x <= input.max_x {
                visit_count.entry(steps).or_insert_with(|| Vec::new()).push(dx);
            }
        }
    }

    for dv in (-200..10000).rev() {
        let mut y = 0;
        let mut vel = dv;
        let mut steps = 0;
        let mut max_reached = 0;

        let mut matching_dx = HashSet::new();

        while y >= input.min_y {
            y += vel;
            if y > max_reached {
                max_reached = y;
            }
            vel -= 1;
            steps += 1;

            if y >= input.min_y && y <= input.max_y {
                if let Some(dxs) = visit_count.get(&steps) {
                    for dx in dxs {
                        matching_dx.insert(*dx);
                    }
                }
            }
        }

        combinations += matching_dx.len();
    }

    combinations
}

fn main() {
    let input = read_input();

    println!("{:?}", &input);
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
