use std::{cmp::Reverse, collections::{hash_map::Entry, BinaryHeap, HashMap}, io::BufRead};

type Coords = (i32, i32);
type Map = HashMap<Coords, i32>;

fn read_input() -> Map {
    std::io::stdin()
        .lock()
        .lines()
        .enumerate()
        .flat_map(|(row, line)| {
            let line = line.unwrap();

            line.chars()
                .enumerate()
                .map(|(col, c)| ((row as i32, col as i32), c.to_digit(10).unwrap() as i32))
                .collect::<Map>()
        })
        .collect()
}

fn abs(v: i32) -> i32 {
    if v > 0 {
        v
    } else {
        -v
    }
}

fn solve(input: &Map) -> i32 {
    let mut pq: BinaryHeap<Reverse<(i32, Coords)>> = BinaryHeap::new();
    let mut distances: HashMap<Coords, i32> = HashMap::new();

    pq.push(Reverse((0, (0, 0))));
    distances.insert((0, 0), 0);

    let destination = *input.keys().max().unwrap();

    while let Some(Reverse((distance, coords))) = pq.pop() {
        if *distances.get(&coords).unwrap() < distance {
            continue;
        }
        if coords == destination {
            return distance;
        }

        for dy in -1..=1 {
            for dx in -1..=1 {
                if abs(dy) + abs(dx) == 1 {
                    let new_coords = (coords.0 + dy, coords.1 + dx);
                    if let Some(&risk) = input.get(&new_coords) {
                        let new_risk = distance + risk;

                        let updated = match distances.entry(new_coords) {
                            Entry::Occupied(mut e) => {
                                let distance = e.get_mut();
                                if new_risk < *distance {
                                    *distance = new_risk;
                                    true
                                } else {
                                    false
                                }
                            }
                            Entry::Vacant(e) => {
                                e.insert(new_risk);
                                true
                            }
                        };

                        if updated {
                            pq.push(Reverse((new_risk, new_coords)));
                        }
                    }
                }
            }
        }
    }

    0
}

fn expand_input(input: &Map) -> Map {
    let mut expanded = Map::new();

    let (height, width) = *input.keys().max().unwrap();

    for (&(x, y), &risk) in input {
        for dy in 0..5 {
            for dx in 0..5 {
                expanded.insert((dy * (height + 1) + y, dx * (width + 1) + x), (risk + dx + dy - 1) % 9 + 1);
            }
        }
    }

    expanded
}

fn part2(input: &Map) -> i32 {
    let expanded = expand_input(input);

    solve(&expanded)
}

fn main() {
    let input = read_input();

    println!("part1: {}", solve(&input));
    println!("part2: {}", part2(&input));
}
