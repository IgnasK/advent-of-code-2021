
use core::panic;
use std::io::{self, BufRead};

#[derive(Debug)]
enum Command {
    Forward(i32),
    Down(i32),
    Up(i32),
}

fn read_input() -> Vec<Command> {
    let mut commands: Vec<Command> = vec![];

    for line in io::stdin().lock().lines() {
        let line = line.unwrap();
        let mut components = line.split_whitespace();

        let command_verb = components.next().unwrap();
        let command_number = components.next().unwrap().parse::<i32>().unwrap();
        commands.push(
        match command_verb {
            "forward" => Command::Forward(command_number),
            "up" => Command::Up(command_number),
            "down" => Command::Down(command_number),
            _ => panic!("unknown command"),
        }
    )
    }

    commands
}

fn part1(input: &Vec<Command>) -> i64 {
    let mut depth: i64 = 0;
    let mut distance: i64 = 0;

    for c in input {
        match c {
            &Command::Forward(v) => distance += v as i64,
            &Command::Up(v) => depth -= v as i64,
            &Command::Down(v) => depth += v as i64,
        }
    }

    depth * distance
}

fn part2(input: &Vec<Command>) -> i64 {
    let mut aim: i64 = 0;
    let mut depth: i64 = 0;
    let mut distance: i64 = 0;

    for c in input {
        match c {
            &Command::Forward(v) => {
                distance += v as i64;
                depth += v as i64 * aim;
            },
            &Command::Up(v) => aim -= v as i64,
            &Command::Down(v) => aim += v as i64,
        }
    }

    depth * distance
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
