use bit_vec::BitVec;
use std::io::Read;

#[derive(Debug)]
enum OpType {
    Sum,
    Product,
    Min,
    Max,
    Gt,
    Lt,
    Eq,
}

#[derive(Debug)]
enum Content {
    Literal(u64),
    Op(OpType, Vec<Packet>),
}

#[derive(Debug)]
struct Packet {
    version: u64,
    content: Content,
}

fn read_input() -> BitVec {
    let mut result = BitVec::new();

    let mut input = String::new();
    std::io::stdin().lock().read_to_string(&mut input).unwrap();

    for c in input.chars() {
        let v = match c {
            '0'..='9' => c as u32 - '0' as u32,
            'A'..='Z' => c as u32 - 'A' as u32 + 10,
            _ => 0,
        };

        for i in (0..4).rev() {
            result.push((v >> i) & 1 == 1);
        }
    }

    result
}

fn parse_int(input: &BitVec, idx: usize, len: usize) -> u64 {
    let mut result = 0;
    for i in idx..(idx + len) {
        result = result << 1;
        if let Some(true) = input.get(i) {
            result |= 1;
        }
    }

    result
}

fn parse_literal(input: &BitVec, idx: usize) -> (u64, usize) {
    let mut result = 0;
    let mut idx = idx;

    loop {
        let cont = parse_int(input, idx, 1);
        let val = parse_int(input, idx + 1, 4);

        result = result << 4 | val;

        idx += 5;
        if cont == 0 {
            break;
        }
    }

    (result, idx)
}

fn parse_subpackets(input: &BitVec, idx: usize) -> (Vec<Packet>, usize) {
    let mut idx = idx;

    let length_id = parse_int(input, idx, 1);

    let mut subpackets = Vec::new();

    match length_id {
        0 => {
            let length = parse_int(input, idx + 1, 15);

            idx = idx + 16;

            let end = idx + length as usize;
            while idx < end {
                let (packet, sub_end) = parse(input, idx);
                subpackets.push(packet);
                idx = sub_end;
            }
        }
        1 => {
            let count = parse_int(input, idx + 1, 11);

            idx = idx + 12;

            for _ in 0..count {
                let (packet, sub_end) = parse(input, idx);
                subpackets.push(packet);
                idx = sub_end;
            }
        }
        _ => unreachable!(),
    }

    (subpackets, idx)
}

fn parse(input: &BitVec, idx: usize) -> (Packet, usize) {
    let mut idx = idx;

    let version = parse_int(input, idx, 3);
    let id = parse_int(input, idx + 3, 3);

    let content: Content = match id {
        4 => {
            let (literal, end) = parse_literal(input, idx + 6);
            idx = end;
            Content::Literal(literal)
        }
        _ => {
            let (subpackets, end) = parse_subpackets(input, idx + 6);
            idx = end;

            let op_type = match id {
                0 => OpType::Sum,
                1 => OpType::Product,
                2 => OpType::Min,
                3 => OpType::Max,
                5 => OpType::Gt,
                6 => OpType::Lt,
                7 => OpType::Eq,
                _ => unreachable!(),
            };

            Content::Op(op_type, subpackets)
        }
    };

    (Packet { version, content }, idx)
}

fn version_sum(packet: &Packet) -> u64 {
    match &packet.content {
        Content::Literal(_) => packet.version,
        Content::Op(_, sub) => packet.version + sub.iter().map(|p| version_sum(p)).sum::<u64>(),
    }
}

fn eval(packet: &Packet) -> u64 {
    match &packet.content {
        Content::Literal(v) => *v,
        Content::Op(OpType::Sum, sub) => sub.iter().map(|p| eval(p)).sum(),
        Content::Op(OpType::Product, sub) => sub.iter().map(|p| eval(p)).fold(1, |acc, v| acc * v),
        Content::Op(OpType::Min, sub) => sub.iter().map(|p| eval(p)).min().unwrap(),
        Content::Op(OpType::Max, sub) => sub.iter().map(|p| eval(p)).max().unwrap(),
        Content::Op(OpType::Gt, sub) => {
            if eval(&sub[0]) > eval(&sub[1]) {
                1
            } else {
                0
            }
        }
        Content::Op(OpType::Lt, sub) => {
            if eval(&sub[0]) < eval(&sub[1]) {
                1
            } else {
                0
            }
        }
        Content::Op(OpType::Eq, sub) => {
            if eval(&sub[0]) == eval(&sub[1]) {
                1
            } else {
                0
            }
        }
    }
}

fn main() {
    let input = read_input();
    let (parsed, _) = parse(&input, 0);

    println!("{:?}", &parsed);
    println!("part1: {:?}", version_sum(&parsed));
    println!("part2: {:?}", eval(&parsed));
}
