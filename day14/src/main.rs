use std::{collections::HashMap, io::BufRead, iter::FromIterator};

#[macro_use]
extern crate scan_fmt;

#[derive(Debug)]
struct Rule {
    pattern: String,
    replacements: Vec<String>,
}

fn read_input() -> (String, Vec<Rule>) {
    let stdin = std::io::stdin();
    let mut lines = stdin.lock().lines().map(|l| l.unwrap());

    let input = lines.next().unwrap();
    let mut rules = Vec::new();

    lines.skip(1).for_each(|line| {
        if let Ok((pattern, replacement)) = scan_fmt!(&line, "{} -> {}", String, char) {
            rules.push(Rule {
                pattern: pattern.clone(),
                replacements: vec![
                    String::from_iter([pattern.chars().next().unwrap(), replacement]),
                    String::from_iter([replacement, pattern.chars().nth(1).unwrap()]),
                ],
            })
        }
    });

    (input, rules)
}

fn step(
    pairs: &HashMap<String, i64>,
    rules: &HashMap<String, Vec<String>>,
) -> HashMap<String, i64> {
    let mut result = HashMap::new();

    for (key, &count) in pairs {
        match rules.get(key) {
            Some(replacements) => {
                for rep in replacements {
                    *result.entry(rep.clone()).or_insert(0) += count;
                }
            }
            None => *result.entry(key.clone()).or_insert(0) += count,
        }
    }

    result
}

fn run(input: &String, rules: &Vec<Rule>, times: usize) -> i64 {
    let mut pairs: HashMap<String, i64> = HashMap::new();

    input.chars().zip(input.chars().skip(1)).for_each(|(a, b)| {
        *pairs.entry(String::from_iter([a, b])).or_insert(0) += 1;
    });

    pairs.insert(String::from(input.chars().next().unwrap()), 1);
    pairs.insert(String::from(input.chars().last().unwrap()), 1);

    let rules_map = rules
        .iter()
        .map(|r| (r.pattern.clone(), r.replacements.clone()))
        .collect();

    for _ in 0..times {
        pairs = step(&pairs, &rules_map);
    }

    // Double counted
    let mut counts = HashMap::<char, i64>::new();

    pairs.iter().for_each(|(k, &v)| {
        k.chars().for_each(|c| {
            *counts.entry(c).or_insert(0) += v;
        })
    });

    let mut sorted_counts: Vec<(char, i64)> = counts.into_iter().collect();
    sorted_counts.sort_by(|&a, &b| a.1.cmp(&b.1));

    (sorted_counts.last().unwrap().1 - sorted_counts.first().unwrap().1) / 2
}

fn main() {
    let (input, rules) = read_input();

    println!("part1: {}", run(&input, &rules, 10));
    println!("part2: {}", run(&input, &rules, 40));
}
