use std::{
    collections::{HashMap, HashSet},
    io::BufRead,
};

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
enum Node {
    Start,
    End,
    Big(String),
    Small(String),
}

type Graph = HashMap<Node, Vec<Node>>;

fn parse_node(name: String) -> Node {
    match &name as &str {
        "start" => Node::Start,
        "end" => Node::End,
        _ if name.chars().all(|c| c.is_uppercase()) => Node::Big(name),
        _ => Node::Small(name),
    }
}

fn read_input() -> Graph {
    let mut graph = Graph::new();

    std::io::stdin().lock().lines().for_each(|line| {
        let line = line.unwrap();

        let mut iter = line.split("-");

        let start = parse_node(iter.next().unwrap().to_owned());
        let end = parse_node(iter.next().unwrap().to_owned());

        graph
            .entry(start.clone())
            .or_insert_with(|| Vec::new())
            .push(end.clone());
        graph.entry(end).or_insert_with(|| Vec::new()).push(start);
    });

    graph
}

fn walk(graph: &Graph, node: &Node, visited: &mut HashSet<Node>) -> usize {
    if matches!(node, Node::Small(_) | Node::Start) && visited.contains(node) {
        return 0;
    }
    if let Node::End = node {
        return 1;
    }

    visited.insert(node.clone());

    let mut total = 0;

    for neighbour in graph.get(node).unwrap() {
        total += walk(graph, neighbour, visited);
    }

    visited.remove(node);

    total
}

fn walk_multivisit(graph: &Graph, node: &Node, visited: &mut HashMap<Node, usize>) -> usize {
    if matches!(node, Node::Start) && *visited.get(node).unwrap_or(&0) >= 1 {
        return 0; // Cannot visit start more than once
    }
    if let Node::End = node {
        return 1;
    }

    *visited.entry(node.clone()).or_insert(0) += 1;

    if matches!(node, Node::Small(_)) {
        if visited
            .iter()
            .any(|(node, &v)| matches!(node, &Node::Small(_)) && v > 2) // any node visited more than twice
            || visited
                .iter()
                .filter(|(node, &v)| matches!(node, &Node::Small(_)) && v == 2)
                .count()
                > 1 // More than one node visited twice
        {
            *visited.get_mut(node).unwrap() -= 1;
            return 0;
        }
    }

    let mut total = 0;

    for neighbour in graph.get(node).unwrap() {
        total += walk_multivisit(graph, neighbour, visited);
    }

    *visited.get_mut(node).unwrap() -= 1;

    total
}

fn part1(graph: &Graph) -> usize {
    let mut visited = HashSet::new();
    walk(graph, &Node::Start, &mut visited)
}

fn part2(graph: &Graph) -> usize {
    let mut visited = HashMap::new();
    walk_multivisit(graph, &Node::Start, &mut visited)
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
