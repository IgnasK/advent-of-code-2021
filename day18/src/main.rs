use std::io::BufRead;

#[derive(Debug, Clone, Copy)]
struct Value {
    number: i64,
    depth: usize,
}

type Number = Vec<Value>;

fn read_input() -> Vec<Number> {
    std::io::stdin()
        .lock()
        .lines()
        .map(|l| {
            let l = l.unwrap();

            let mut number = Vec::new();

            let mut depth = 0;

            for c in l.chars() {
                match c {
                    '[' => depth += 1,
                    ']' => depth -= 1,
                    '0'..='9' => number.push(Value {
                        number: c as i64 - '0' as i64,
                        depth,
                    }),
                    _ => (),
                }
            }

            number
        })
        .collect()
}

fn explode(n: Number) -> (Number, bool) {
    let mut n = n;
    let mut changed = false;

    let mut i = 0;
    while i < n.len() {
        if n[i].depth == 5 {
            let a = n[i].number;
            let b = n[i + 1].number;
            if i > 0 {
                if let Some(previous) = n.get_mut(i - 1) {
                    previous.number += a;
                }
            }

            if let Some(next) = n.get_mut(i + 2) {
                next.number += b;
            }
            n[i].depth -= 1;
            n[i].number = 0;
            n.remove(i + 1);
            changed = true;
        }

        i += 1;
    }

    (n, changed)
}

fn split(n: Number) -> (Number, bool) {
    let mut n = n;

    let mut i = 0;
    while i < n.len() {
        let value = n[i];
        if value.number > 9 {
            n[i].number = value.number / 2;
            n[i].depth += 1;

            n.insert(
                i + 1,
                Value {
                    number: (value.number + 1) / 2,
                    depth: value.depth + 1,
                },
            );
            return (n, true);
        }
        i += 1;
    }

    (n, false)
}

fn add(a: Number, b: Number) -> Number {
    let mut a = a;
    a.extend(b.into_iter());

    for mut v in a.iter_mut() {
        v.depth += 1;
    }

    a
}

fn simplify(n: Number) -> Number {
    let mut n = n;

    loop {
        let (next, _) = explode(n);
        n = next;
        let (next, changed) = split(n);
        n = next;
        if !changed {
            return n;
        }
    }
}

fn eval(n: Number) -> i64 {
    let mut magnitudes: Vec<Value> = Vec::new();

    for v in n {
        magnitudes.push(v);
        while magnitudes.len() > 1 {
            let a = magnitudes[magnitudes.len() - 2];
            let b = magnitudes[magnitudes.len() - 1];

            if a.depth == b.depth {
                magnitudes.pop();
                magnitudes.pop();
                magnitudes.push(Value {
                    number: a.number * 3 + b.number * 2,
                    depth: a.depth - 1,
                });
            } else {
                break;
            }
        }
    }

    magnitudes[0].number
}

fn part1(input: &Vec<Number>) -> i64 {
    let mut n = input[0].clone();

    for v in input.iter().skip(1) {
        let v = v.clone();

        n = add(n, v);
        n = simplify(n);
    }

    eval(n)
}

fn part2(input: &Vec<Number>) -> i64 {
    let mut best = 0;

    for i in 0..input.len() {
        for j in 0..input.len() {
            if i != j {
                let score = eval(simplify(add(input[i].clone(), input[j].clone())));
                if score > best {
                    best = score;
                }
            }
        }
    }

    best
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
