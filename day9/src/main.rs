use std::{collections::{BinaryHeap, HashMap, HashSet}, io::BufRead};

type Coords = (i32, i32);
type Map = HashMap<Coords, i32>;

fn read_input() -> Map {
    std::io::stdin().lock().lines().enumerate().flat_map(|(row, line)| {
        let line = line.unwrap();

        line.chars().enumerate().map(|(col, c)| ((row as i32, col as i32), c.to_digit(10).unwrap() as i32)).collect::<Map>()
    }).collect()
}

fn part1(input: &Map) -> i32 {
    input.iter().filter(|&(&(row, col), &val)| {
        input.get(&(row-1, col)).map(|&f| f > val).unwrap_or(true) &&
        input.get(&(row+1, col)).map(|&f| f > val).unwrap_or(true) &&
        input.get(&(row, col-1)).map(|&f| f > val).unwrap_or(true) &&
        input.get(&(row, col+1)).map(|&f| f > val).unwrap_or(true)
    }).map(|(_, &val)| val + 1).sum()
}

fn abs(v: i32) -> i32 {
    if v > 0 {
        v
    } else {
        -v
    }
}

fn part2(input: &Map) -> i64 {
    let mut visited: HashSet<Coords> = HashSet::new();

    let mut sizes: BinaryHeap<i32> = BinaryHeap::new();

    for (&coords, &v) in input.iter() {
        if visited.contains(&coords) || v == 9 {
            continue
        }
        visited.insert(coords);

        let mut candidates: Vec<Coords> = Vec::new();
        let mut current_size = 0;

        candidates.push(coords);

        while let Some(coords) = candidates.pop() {
            current_size += 1;
            
            for dy in -1..=1 {
                for dx in -1..=1 {
                    if abs(dx) + abs(dy) == 1 {
                        let new_coords = (coords.0 + dy, coords.1 + dx);

                        if !visited.contains(&new_coords) && input.get(&new_coords).map_or(false, |&v| v != 9) {
                            visited.insert(new_coords);
                            candidates.push(new_coords);
                        }
                    }
                }
            }
        }

        sizes.push(current_size);

    }

    sizes.iter().take(3).fold::<i64, _>(1, |v, &cur| cur as i64 * v)
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}