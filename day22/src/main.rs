use std::{
    collections::{BTreeSet, HashMap},
    io::BufRead,
};

use scan_fmt::scan_fmt;

type Point = (i64, i64, i64);
type Range = (Point, Point);

#[derive(Debug)]
enum Step {
    On(Range),
    Off(Range),
}

fn read_input() -> Vec<Step> {
    std::io::stdin()
        .lock()
        .lines()
        .map(|line| {
            let line = line.unwrap();

            let (state, min_x, max_x, min_y, max_y, min_z, max_z) = scan_fmt!(
                &line,
                "{} x={}..{},y={}..{},z={}..{}",
                String,
                i64,
                i64,
                i64,
                i64,
                i64,
                i64
            )
            .unwrap();

            match &state as &str {
                "on" => Step::On(((min_x, min_y, min_z), (max_x, max_y, max_z))),
                "off" => Step::Off(((min_x, min_y, min_z), (max_x, max_y, max_z))),
                _ => unreachable!(),
            }
        })
        .collect()
}

fn solve(input: &Vec<Step>) -> (i64, i64) {
    let mut ranges: Vec<BTreeSet<i64>> = Vec::new();
    for _ in 0..3 {
        ranges.push(BTreeSet::new());
    }

    for step in input {
        let range = match step {
            Step::On(r) => r,
            Step::Off(r) => r,
        };
        ranges[0].insert(range.0 .0);
        ranges[0].insert(range.1 .0 + 1);
        ranges[1].insert(range.0 .1);
        ranges[1].insert(range.1 .1 + 1);
        ranges[2].insert(range.0 .2);
        ranges[2].insert(range.1 .2 + 1);
    }

    // part 1
    for dim in 0..3 {
        ranges[dim].insert(-50);
        ranges[dim].insert(51);
    }

    let mut index_to_value: Vec<HashMap<usize, i64>> = Vec::new();
    let mut value_to_index: Vec<HashMap<i64, usize>> = Vec::new();
    for dim in 0..3 {
        index_to_value.push(HashMap::new());
        value_to_index.push(HashMap::new());
        for (i, &value) in ranges[dim].iter().enumerate() {
            index_to_value[dim].insert(i, value);
            value_to_index[dim].insert(value, i);
        }
    }

    let mut blocks: Vec<Vec<Vec<bool>>> =
        vec![
            vec![vec![false; value_to_index[2].len()]; value_to_index[1].len()];
            value_to_index[0].len()
        ];

    for step in input {
        let (r, on) = match step {
            Step::On(r) => (r, true),
            Step::Off(r) => (r, false),
        };

        for x in value_to_index[0][&r.0 .0]..value_to_index[0][&(r.1 .0 + 1)] {
            for y in value_to_index[1][&r.0 .1]..value_to_index[1][&(r.1 .1 + 1)] {
                for z in value_to_index[2][&r.0 .2]..value_to_index[2][&(r.1 .2 + 1)] {
                    blocks[x][y][z] = on;
                }
            }
        }
    }

    let (mut part1, mut part2) = (0, 0);

    for x in 0..value_to_index[0].len() - 1 {
        for y in 0..value_to_index[1].len() - 1 {
            for z in 0..value_to_index[2].len() - 1 {
                if !blocks[x][y][z] {
                    continue;
                }
                let volume = (index_to_value[0][&(x + 1)] - index_to_value[0][&x])
                    * (index_to_value[1][&(y + 1)] - index_to_value[1][&y])
                    * (index_to_value[2][&(z + 1)] - index_to_value[2][&z]);

                if index_to_value[0][&x] >= -50
                    && index_to_value[0][&(x + 1)] <= 51
                    && index_to_value[1][&y] >= -50
                    && index_to_value[1][&(y + 1)] <= 51
                    && index_to_value[2][&z] >= -50
                    && index_to_value[2][&(z + 1)] <= 51
                {
                    part1 += volume;
                }

                part2 += volume;
            }
        }
    }

    (part1, part2)
}

fn main() {
    let input = read_input();

    let (part1, part2) = solve(&input);
    println!("part1: {}", part1);
    println!("part2: {}", part2);
}
