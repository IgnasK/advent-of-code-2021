use std::{collections::HashSet, io::{self, BufRead}};

type Board = Vec<Vec<i32>>;
type Draw = Vec<i32>;

fn read_input() -> (Vec<Board>, Draw) {
    let mut boards: Vec<Board> = vec![];

    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();

    let draw = lines.next().unwrap().unwrap().split(",").map(|v| v.parse().unwrap()).collect();

    let mut current_board: Board = vec![];
    for line in lines {
        let line = line.unwrap();
        if line.is_empty() {
            continue;
        }

        let line_numbers = line.split_whitespace().map(|v| v.parse::<i32>().unwrap()).collect();
        current_board.push(line_numbers);

        if current_board.len() == 5 {
            boards.push(current_board);
            current_board = vec![];
        }

    }

    (boards, draw)
}

fn check_board(board: &Board, draw: &HashSet<i32>) -> bool {
    for col in 0..5 {
        let mut completed = true;
        for row in 0..5 {
            completed &= draw.contains(&board[row][col]);
        }
        if completed {
            return true
        }
    }

    for row in 0..5 {
        let mut completed = true;
        for col in 0..5 {
            completed &= draw.contains(&board[row][col]);
        }
        if completed {
            return true
        }
    }

    false
}

fn part1(boards: &Vec<Board>, draw: &Draw) -> i32 {
    for i in 0..draw.len() {
        let current_draw: HashSet<i32> = draw[0..i+1].iter().cloned().collect();
        for board in boards {
            if check_board(board, &current_draw) {
                let board_numbers: HashSet<i32> = board.iter().flatten().cloned().collect();

                let unused_numbers = &board_numbers - &current_draw;

                return draw[i] * unused_numbers.iter().sum::<i32>();
            }
        }
    }

    0
}

fn part2(boards: &Vec<Board>, draw: &Draw) -> i32 {
    let mut winning: HashSet<usize> = HashSet::new();
    for i in 0..draw.len() {
        let current_draw: HashSet<i32> = draw[0..i+1].iter().cloned().collect();
        for (board_number, board) in boards.iter().enumerate() {
            if winning.contains(&board_number) {
                continue;
            }
            
            if check_board(board, &current_draw) {
                winning.insert(board_number);

                // All boards have won, so this is the last
                if winning.len() == boards.len() {
                    let board_numbers: HashSet<i32> = board.iter().flatten().cloned().collect();

                    let unused_numbers = &board_numbers - &current_draw;

                    return draw[i] * unused_numbers.iter().sum::<i32>();
                }
            }
        }
    }

    0
}

fn main() {
    let (boards, draw) = read_input();

    println!("part1: {}", part1(&boards, &draw));
    println!("part2: {}", part2(&boards, &draw));
}
