use std::io::Read;

fn read_input() -> Vec<i64> {
    let mut input = String::new();
    std::io::stdin().lock().read_to_string(&mut input).unwrap();

    let mut numbers: Vec<i64> = input.split(",").map(|v| v.trim().parse().unwrap()).collect();
    numbers.sort();
    numbers
}

fn abs(v: i64) -> i64 {
    if v > 0 {
        v
    } else {
        -v
    }
}

fn eval(input: &Vec<i64>, target: i64) -> i64 {
    input.iter().map(|pos| abs(pos - target)).sum()
}

fn part1(input: &Vec<i64>) -> i64 {
    let mut start = *input.first().unwrap();
    let mut end = *input.last().unwrap();

    while start < end {
        let m1 = start + (end-start+1)/3;
        let m2 = end - (end-start+1)/3;

        let v1 = eval(input, m1);
        let v2 = eval(input, m2);

        if v1 < v2 {
            end = m2;
        } else if v1 > v2 {
            start = m1;
        } else {
            start = m1;
            end = m2;
        }
    }

    return eval(input, start);
}

fn eval_part2(input: &Vec<i64>, target: i64) -> i64 {
    input.iter().map(|pos| {
        let distance = abs(pos - target);
        distance * (distance + 1) / 2
    }).sum()
}

fn part2(input: &Vec<i64>) -> i64 {
    let mut start = *input.first().unwrap();
    let mut end = *input.last().unwrap();

    while start < end {
        let m1 = start + (end-start+1)/3;
        let m2 = end - (end-start+1)/3;

        let v1 = eval_part2(input, m1);
        let v2 = eval_part2(input, m2);

        // println!("{}, {}:{}, {}:{}, {}", start, m1, v1, m2, v2, end);

        if v1 < v2 {
            end = m2;
        } else if v1 > v2 {
            start = m1;
        } else {
            start = m1;
            end = m2;
        }
    }

    return eval_part2(input, start);
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
