use std::{collections::HashSet, fmt::Write};

#[macro_use]
extern crate scan_fmt;

type Point = (i64, i64);

#[derive(Debug)]
enum Fold {
    Horizontal(i64),
    Vertical(i64),
}

fn read_input() -> (Vec<Point>, Vec<Fold>) {
    let mut line = String::new();

    let mut points = Vec::new();
    let mut folds = Vec::new();

    while let Ok(_) = std::io::stdin().read_line(&mut line) {
        if line.trim().is_empty() {
            break;
        }

        if let Ok((x, y)) = scan_fmt!(&line, "{},{}", i64, i64) {
            points.push((x, y));
        }

        line.clear();
    }

    while let Ok(_) = std::io::stdin().read_line(&mut line) {
        if line.trim().is_empty() {
            break;
        }

        if let Ok((axis, val)) = scan_fmt!(&line, "fold along {}={}", char, i64) {
            match axis {
                'x' => folds.push(Fold::Horizontal(val)),
                'y' => folds.push(Fold::Vertical(val)),
                _ => (),
            }
        }

        line.clear();
    }

    (points, folds)
}

fn abs(v: i64) -> i64 {
    if v > 0 {
        v
    } else {
        -v
    }
}

fn transform(mut point: Point, fold: &Fold) -> Point {
    match fold {
        &Fold::Horizontal(x) => point.0 = x - abs(point.0 - x),
        &Fold::Vertical(y) => point.1 = y - abs(point.1 - y),
    }
    point
}

fn transform_all(mut point: Point, folds: &Vec<Fold>) -> Point {
    for fold in folds {
        point = transform(point, fold);
    }

    point
}

fn part1(points: &Vec<Point>, folds: &Vec<Fold>) -> usize {
    let folded: HashSet<Point> = points.iter().map(|&p| transform(p, &folds[0])).collect();

    folded.len()
}

fn max(a: i64, b: i64) -> i64 {
    if a > b {
        a
    } else {
        b
    }
}

fn part2(points: &Vec<Point>, folds: &Vec<Fold>) -> String {
    let folded: HashSet<Point> = points.iter().map(|&p| transform_all(p, folds)).collect();

    let (max_x, max_y) = folded.iter().fold((0, 0), |(acc_x, acc_y), &(x, y)| {
        (max(acc_x, x), max(acc_y, y))
    });

    let mut result = String::new();

    for y in 0..=max_y {
        for x in 0..=max_x {
            if folded.contains(&(x, y)) {
                result.write_str("██").unwrap();
            } else {
                result.write_str("  ").unwrap();
            }
        }
        result.write_char('\n').unwrap();
    }

    result
}

fn main() {
    let (points, folds) = read_input();

    println!("part1: {}", part1(&points, &folds));
    println!("part2:\n{}", part2(&points, &folds));
}
