use std::{
    collections::{hash_map::Entry, HashMap, HashSet, VecDeque},
    io::BufRead,
};

type Coords = (i32, i32);
type Map = HashMap<Coords, i32>;

fn read_input() -> Map {
    std::io::stdin()
        .lock()
        .lines()
        .enumerate()
        .flat_map(|(row, line)| {
            let line = line.unwrap();

            line.chars()
                .enumerate()
                .map(|(col, c)| ((row as i32, col as i32), c.to_digit(10).unwrap() as i32))
                .collect::<Map>()
        })
        .collect()
}

fn step(input: &Map) -> (Map, i32) {
    let mut new_map: Map = input.iter().map(|(&k, &v)| (k, v + 1)).collect();

    let mut visited: HashSet<Coords> = HashSet::new();
    let mut queue: VecDeque<Coords> = VecDeque::new();

    new_map.iter().for_each(|(&coords, &value)| {
        if value > 9 {
            visited.insert(coords);
            queue.push_back(coords);
        }
    });

    while let Some(coords) = queue.pop_front() {
        new_map.insert(coords, 0);

        for dy in -1..=1 {
            for dx in -1..=1 {
                if dy != 0 || dx != 0 {
                    let new_coords = (coords.0 + dy, coords.1 + dx);

                    if visited.contains(&new_coords) {
                        continue;
                    }

                    if let Entry::Occupied(mut entry) = new_map.entry(new_coords) {
                        *entry.get_mut() += 1;

                        if *entry.get() > 9 {
                            visited.insert(new_coords);
                            queue.push_back(new_coords);
                        }
                    }
                }
            }
        }
    }

    (new_map, visited.len() as i32)
}

#[allow(dead_code)]
fn print_map(input: &Map) {
    for y in 0..10 {
        for x in 0..10 {
            print!("{}", input.get(&(y, x)).unwrap());
        }
        println!();
    }
}

fn part1(input: &Map) -> i32 {
    let mut flashes = 0;

    let mut map = input.clone();

    for _ in 0..100 {
        // print_map(&map);
        // println!();

        let (new_map, new_flashes) = step(&map);
        map = new_map;
        flashes += new_flashes;
    }

    flashes
}

fn part2(input: &Map) -> i32 {
    let mut map = input.clone();

    let mut i = 1;
    loop {
        let (new_map, new_flashes) = step(&map);
        map = new_map;

        if new_flashes == map.len() as i32 {
            return i;
        }

        i += 1;
    }
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
