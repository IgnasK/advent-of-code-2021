use std::io::{self, BufRead};

fn read_input() -> Vec<i64> {

    io::stdin()
        .lock()
        .lines()
        .map::<Vec<i64>, _>(
            |l| {
                let l = l.unwrap();
                l.split(",")
                .map(
                    |v|
                    v.parse::<i64>().unwrap()
                ).collect()
            }
            )
        .flatten()
        .collect()
}

fn step(input: &mut [i64; 9]) -> [i64; 9] {
    let current = input[0];

    for i in 0..8 {
        input[i] = input[i+1];
    }

    input[8] = current;
    input[6] += current;

    *input
}

fn histogram(input: &Vec<i64>) -> [i64; 9] {
    let mut hist = [0; 9];

    for v in input {
        hist[*v as usize] += 1;
    }

    hist
}

fn simulate(input: &Vec<i64>, iter: usize) -> i64 {
    let mut hist = histogram(input);

    for _ in 0..iter {
        hist = step(&mut hist);
    }

    hist.iter().sum()
}

fn part1(input: &Vec<i64>) -> i64 {
    simulate(input, 80)
}

fn part2(input: &Vec<i64>) -> i64 {
    simulate(input, 256)
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
