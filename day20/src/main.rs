use std::{collections::HashSet, io::BufRead};

type Coords = (i64, i64);
type Map = HashSet<Coords>;
type Mapping = Vec<char>;

fn read_input() -> (Mapping, Map) {
    let mut mapping = String::new();

    std::io::stdin().read_line(&mut mapping).unwrap();

    let mut map = Map::new();

    for (y, line) in std::io::stdin().lock().lines().enumerate() {
        let line = line.unwrap();

        for (x, c) in line.chars().enumerate() {
            if c == '#' {
                map.insert((x as i64, y as i64));
            }
        }
    }

    (mapping.chars().collect(), map)
}

fn step(mapping: &Mapping, map: &Map, background: char) -> (Map, char) {
    let (mut min_x, mut min_y) = map.iter().next().unwrap();
    let (mut max_x, mut max_y) = (min_x, min_y);

    for &(x, y) in map {
        if min_x > x {
            min_x = x;
        }
        if min_y > y {
            min_y = y;
        }
        if max_x < x {
            max_x = x;
        }
        if max_y < y {
            max_y = y;
        }
    }

    let mut new_map = Map::new();

    for x in min_x - 1..=max_x + 1 {
        for y in min_y - 1..=max_y + 1 {
            let mut index = 0;
            for dy in -1..=1 {
                for dx in -1..=1 {
                    let new_point = (x + dx, y + dy);
                    index <<= 1;
                    if map.contains(&new_point)
                        || (new_point.0 < min_x
                            || new_point.0 > max_x
                            || new_point.1 < min_y
                            || new_point.1 > max_y)
                            && background == '#'
                    {
                        index |= 1;
                    }
                }
            }

            if mapping[index] == '#' {
                new_map.insert((x, y));
            }
        }
    }

    let new_background = if background == '.' {
        mapping[0]
    } else {
        mapping[511]
    };

    (new_map, new_background)
}

fn part1(mapping: &Mapping, initial_map: &Map) -> usize {
    let mut map = initial_map.clone();
    let mut background = '.';

    for _ in 0..2 {
        let (new_map, new_background) = step(mapping, &map, background);
        map = new_map;
        background = new_background;
    }

    map.len()
}

fn part2(mapping: &Mapping, initial_map: &Map) -> usize {
    let mut map = initial_map.clone();
    let mut background = '.';

    for _ in 0..50 {
        let (new_map, new_background) = step(mapping, &map, background);
        map = new_map;
        background = new_background;
    }

    map.len()
}

fn main() {
    let (mapping, map) = read_input();

    println!("part1: {}", part1(&mapping, &map));
    println!("part2: {}", part2(&mapping, &map));

}
