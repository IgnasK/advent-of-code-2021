use std::{collections::HashMap, io::{self, BufRead}};

#[macro_use] extern crate scan_fmt;

type Coord = (i32, i32);

#[derive(Debug)]
struct Line {
    start: Coord,
    end: Coord
}

fn read_input() -> Vec<Line> {
    io::stdin().lock().lines().map(|line| {
        let line = line.unwrap();

        let (x1, y1, x2, y2) = scan_fmt!(&line, "{},{} -> {},{}", i32, i32, i32, i32).unwrap();

        Line { start: (x1, y1), end: (x2, y2) }
    }).collect()
}

fn gcd(a: i32, b: i32) -> i32 {
    if b == 0 {
        a
    } else {
        gcd(b, a % b)
    }
}

fn abs(v: i32) -> i32 {
    if v > 0 {
        v
    } else {
        -v
    }
}

fn delta(line: &Line) -> (i32, i32) {
    let dx = line.end.0 - line.start.0;
    let dy = line.end.1 - line.start.1;
    let factor = abs(gcd(dx, dy));

    (dx / factor, dy / factor)
}

fn draw_straight(lines: &Vec<Line>, skip_diagonals: bool) -> HashMap<Coord, i32> {
    let mut map: HashMap<Coord, i32> = HashMap::new();

    for line in lines {
        if skip_diagonals && line.start.0 != line.end.0 && line.start.1 != line.end.1 {
            // not straight
            continue;
        }
        

        let (mut x, mut y) = line.start;
        let (dx, dy) = delta(line);
        

        loop {
            *map.entry((x, y)).or_insert(0) += 1;

            if x == line.end.0 && y == line.end.1 {
                break;
            }

            x += dx;
            y += dy;
        }
    }

    map
}

fn part1(lines: &Vec<Line>) -> i32 {
    let map = draw_straight(lines, true);

    map.values().into_iter().filter(|&&v| v > 1).count() as i32
}

fn part2(lines: &Vec<Line>) -> i32 {
    let map = draw_straight(lines, false);

    map.values().into_iter().filter(|&&v| v > 1).count() as i32
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
