use std::io::BufRead;


fn read_input() -> Vec<String> {
    std::io::stdin().lock().lines().map(|l| l.unwrap()).collect()
}

fn score(c: char) -> i64 {
    match c {
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
        _ => 0,
    }
}

fn part1(input: &Vec<String>) -> i64 {
    let mut total = 0;

    input.iter().for_each(|line| {
        let mut stack: Vec<char> = Vec::new();

        for c in line.chars() {
            match (stack.last(), c) {
                (_, '(' | '[' | '<' | '{') => {stack.push(c);},
                (Some('('), ')') => {stack.pop();},
                (Some('<'), '>') => {stack.pop();},
                (Some('['), ']') => {stack.pop();},
                (Some('{'), '}') => {stack.pop();},
                _ => {total += score(c); break},
            }
        }
    });

    total
}

fn part2(input: &Vec<String>) -> i64 {

    let mut scores: Vec<i64> = Vec::new();

    input.iter().for_each(|line| {
        let mut stack: Vec<char> = Vec::new();

        let mut failed = false;

        for c in line.chars() {
            match (stack.last(), c) {
                (_, '(' | '[' | '<' | '{') => {stack.push(c);},
                (Some('('), ')') => {stack.pop();},
                (Some('<'), '>') => {stack.pop();},
                (Some('['), ']') => {stack.pop();},
                (Some('{'), '}') => {stack.pop();},
                _ => {failed = true; break},
            }
        }

        if failed {
            return;
        }

        scores.push(stack.iter().rev().fold(0, |acc, x| {
            acc * 5 + match x {
                '(' => 1,
                '[' => 2,
                '{' => 3,
                '<' => 4,
                _ => 0,
            }
        }))
    });

    scores.sort();

    scores[scores.len()/2]
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
